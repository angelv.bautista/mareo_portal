<div class="modal fade" id="modalLogon"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <center>Login de usuario</center>
                </h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" name="logon" action="administrator/login.php" method="POST" >
                    <div class="form-group">
                        <label class="control-label col-md-4" for="first_name">
                            Username:
                        </label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="name" name="user" placeholder="nombre de usuario"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="password">
                            Password:
                        </label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" id="userpassword" name="userpassword" placeholder="contrase&#241;a de usuario"/>
                        </div>
                    </div>
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        if (!$logonSuccess)
                            echo "Nombre y/o password invalidos";
                    }
                    ?>
                    <div class="form-group">
                        <div class="col-md-6">

                            <input type="submit" value="Entrar" class="btn btn-custom pull-right">
                        </div>
                    </div>
                </form>
            </div><!-- End of Modal body -->
        </div><!-- End of Modal -->
    </div><!-- End of Modal dialog -->
</div><!-- End of Modal -->
