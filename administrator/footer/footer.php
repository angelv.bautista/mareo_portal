<div id="footer_bg">
    <div class="row">
        <div class="col-sm-6 col-md-6 visible-md visible-lg">
            <h3>Enlaces</h3>
            <ul>
                <li><a href="https://www.geofisica.unam.mx/igualdad/" target="_blank">Igualdad de género</a>  </li>
                <li><a href="http://www.transparencia.unam.mx/" target="_blank">Portal de transparencia universitaria</a></li>
            </ul>
        </div>
        <div class="col-sm-6 col-md-6 visible-md visible-lg">
            <h3>Solicitud de datos</h3>
            <p>
                Si usted requiere datos del Servicio Mareográfico Nacional, favor de descargar y llenar el siguiente formato:
            </p>

        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <br>
    <div class="row">

        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
            <div align="right">

                &nbsp;&nbsp;&nbsp;

                &nbsp;&nbsp;&nbsp;
            </div>
        </div>
    </div>
</div>