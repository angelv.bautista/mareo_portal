<?php
  session_start();
  if (!array_key_exists("user", $_SESSION)){
      header('Location: login.php');
      
      exit;
  }
   
  require_once ('../controladores/common.php');
  $page = empty($_REQUEST['page']) ? 'home' : preg_replace('/[^a-zA-Z0-9\-_]/', '', $_REQUEST['page']);
  $pagepath = "includes/$page.class.php";
  if (file_exists($pagepath)){
      require_once ($pagepath);
      $pageclass = $page . 'Class';
  }
  else{
      $pageclass = 'BasePage';
  }
  $mainframe = new $pageclass($page)
?>
<!DOCTYPE html>
  <html>
  <head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <title><?php $mainframe->title(); ?></title>
    <meta name="description" content="Red Universitaria de Observatorios Atmosféricos">                
 
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/mgmenu.css">
    <link rel="stylesheet" href="css/site.css">
    <link rel="shortcut icon" href="favicon.ico" />

  </head>
  <body id="<?php echo $page;?>_page">

    <header role="banner">
      <div class="container">
          <?php
          include("header/logos.php");
          ?>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <?php
              include("header/menu.php");
            ?>
          </div>
        </div>
      </div>
    </header>
    <main role="main">
      <div class="container">
        <?php $mainframe->render();?>
      </div>
    </main>
    <footer role="contentinfo">
      <div class="container">
        <?php
          include("footer/footer.php");
        ?>
      </div>
    </footer>
    
    
    <script src="js/vendor/jquery-1.10.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-ui.min"><\/script>')</script>
    <script src="js/plugins.js"></script>
    <script src="js/modernizr.custom.49511.js"></script>
    
    <script src="js/main.js"></script>
    <script src="js/mgmenu_plugins.js"></script>
    <script src=" js/validate.js"></script>
    <script src="js/mgmenu.js"></script>
    <script src="ie10-viewport-bug-workaround.js"></script>
    
  </body>
</html>
