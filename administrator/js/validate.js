function addNoticeForm(){
   

		$('.error').hide(0);
		var title = $('input#title').val();
		if (title == "" || title == " " || title == "Title") {
		    $('input#title').focus().before('<div class="error">Por favor escribe un t&iacute;tulo para la noticia</div>');
		    return 0;
		}
                
                var description = $('#description').val();
		if (description == "" || description == " " || description == "Description") {
		    $('#description').focus().before('<div class="error">Olvid&oacute; escribir una descripci&oacute;n para la noticia</div>');
		    return 0;
		}
		
                var link = $('#link').val();
		if (link == "" || link == " " || link == "Link") {
		    $('#link').focus().before('<div class="error">Escriba un v&iacute;nculo a la noticia</div>');
		    return 0;
		}
                
                
              
                document.addNewNotice.submit();

	

}

function comprobar(){
    var x;
    var r = confirm("Estás seguro de eliminar?");
    if(r==true){
        x="Se ha eliminado el registro";
        document.deleteNews.submit();
    }
    
}