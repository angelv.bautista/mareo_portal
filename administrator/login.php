<?php
  require_once("../modelos/db.php");
  $logonSuccess = false;
  
  if ($_SERVER['REQUEST_METHOD'] == "POST") {
      $logonSuccess = (mareograficoDB::getInstance()->verify_user_credentials($_POST['user'], $_POST['userpassword']));
      if ($logonSuccess == true) {
          session_start();
          $_SESSION['user'] = $_POST['user'];
          header('Location: index.php');
          exit;
      }
  }
  
  ?>
<!DOCTYPE html>
  <html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <title>Log In</title>
    <meta name="description" content="Red Universitaria de Observatorios Atmosféricos">                
 
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/mgmenu.css">
    <link rel="stylesheet" href="css/site.css">
    <link rel="shortcut icon" href="favicon.ico" />

  </head>
  <body>

    <header role="banner">
      <div class="container">
        <?php
          include("header/logos.php");
        ?>
      </div>
    </header>
    <main role="main">
        <div class="container">
            <div class="wrapper">
                <br>
                <div class="padre">
                    <div class="hijo">
                        <form name="logon" action="login.php" method="POST">
                            <div class="form-group">
                                <label for="inputEmail">
                                    Username:
                                </label>
                                <input type="text" class="form-control" id="name" name="user" placeholder="nombre de usuario">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">
                                    Password:
                                </label>
                                <input type="password" class="form-control" id="userpassword" name="userpassword" placeholder="contraseña de usuario">
                            </div>
                            <?php
                                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                                    if (!$logonSuccess)
                                        echo "<font color='red'>Nombre y/o password invalidos</font>";
                                }
                            ?>
                            <div class="checkbox">
                                <label><input type="checkbox">Recuerdame</label>
                            </div>
                            <input type="submit" value="Entrar" class="btn btn-info ">

                        </form>

                    </div>

                </div>    
            </div>

        </div>

    </main>
    <footer role="contentinfo">
      <div class="container">
        <?php
          include("footer/footer.php");
        ?>
      </div>
    </footer>
  
    
    
    
    <script src="js/vendor/jquery-1.10.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-ui.min"><\/script>')</script>
    <script src="js/plugins.js"></script>
    <script src="js/modernizr.custom.49511.js"></script>
    
    <script src="js/main.js"></script>
    <script src="js/mgmenu_plugins.js"></script>
    <script src="js/mgmenu.js"></script>
    <script src="ie10-viewport-bug-workaround.js"></script>
    
    
    
    
  </body>
</html>
