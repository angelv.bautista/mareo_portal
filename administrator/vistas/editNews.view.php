<div class="wrapper">
    <div id="frame_wrapper">
        <ol class="breadcrumb visible-sm visible-md visible-lg">
            <li>Usted est&aacute; aqu&iacute;: <a href="index.php?page=home">Reportes de Eventos</a></li>
            <li class="active">Editar</li>
        </ol>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="page-contents">
                    <h3><font color="167ac3"><strong>Editar Reportes</strong></font></h3>
                    <br>
                    <div class="table-responsive">
                        <form name="updateNews" action="index.php?page=updateNews" method="POST">
                            <table class="table-condensed">
                                <?php   
                                    require_once("../modelos/db.php");
           
                                    $result = mareograficoDB::getInstance()->get_news_by_news_id($_POST["newsID"]);
            
                                     while ($row = mysqli_fetch_array($result)):
                                        echo"<tr>";
                                        echo"<td>";
                                        echo"<div class='form-group'>";
                                        echo"<input type='hidden' id='id' name='id' value=".$row['id']." />";
                                        echo"<label for='category' class='control-label col-xs-6 col-sm-6 col-md -6 col-lg-6'>Categor&iacute;a:</label>";
                                        echo"<div class='col-xs-6 col-sm-6 col-md -6 col-lg-6'>";
                                        echo"<select name='categoria' class='form-control'>";
                                        echo"<option value='publicaciones'>Publicaciones y Noticias</option>";
                                        echo"<option value='cursos'>Cursos</option>";
                                        echo"</select><br>";
                                        echo"</div>";
                                        echo"</div>";
                                        echo"<div class='form-group'>";
                                        echo"<label for='titulo' class='control-label col-xs-6 col-md-6 col-sm-6 col-md -6 col-lg-6'>";
                                        echo"T&iacute;tulo";
                                        echo"<span class='required'> *</span>";
                                        echo"</label>";
                                        echo"<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>";
                                        echo"<textarea name='titulo' id ='titulo' rows='1' class='form-control'>".$row['title']."</textarea><br>"; 
                                        echo"</div>";
                                        echo"</div>";
                                        echo"<div class='form-group'>";
                                        echo"<label for='descripcion' class='control-label col-xs-6 col-sm-6 col-md-6 col-lg-6'>";
                                        echo"Descripci&oacute;n";
                                        echo"<span class='required'> *</span>";
                                        echo"</label>";
                                        echo"<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>";
                                        echo"<textarea  name='descripcion' id='descripcion' rows='5' class='form-control'>".$row['short_description']."</textarea><br>";
                                        echo"</div>";
                                        echo"</div>";
                                        echo"<div class='form-group'>";
                                        echo"<label class='control-label col-xs-6 col-sm-6 col-md-6 col-lg-6' for='link'>";
                                        echo"V&iacute;nculo noticia";
                                        echo"<span class='required'> *</span>";
                                        echo"</label>";
                                        echo"<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>";
                                        echo"<input type='input' class='form-control' id='liga' name='liga' value=".$row['link1']."/>";
                                        echo"</div>";
                                        echo"</div>";
                                        
                                        $newsID = $row["id"];
                                        $categoria = $row["category"];
                                        $titleN = $row["title"];
                                        
                                        echo"</td>";
                                        echo "</tr>";
                                        echo "<tr>";
                                    ?>
                                    <td>
                                        
                                        <input type="hidden" name="newsID" value="<?php echo $newsID; ?>" />
                                        <input type="submit" name="editNew" class="btn btn-warning btn-sm" value="Actualizar" />
                                    </td>
                                    <?php
                                        echo "</tr>\n";
                                        endwhile;
                                        mysqli_free_result($result);
                                    ?>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>    
    </div> 
</div>
