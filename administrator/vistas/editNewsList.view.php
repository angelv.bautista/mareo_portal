
<div class="wrapper">
  <div id="">
    <ol class="breadcrumb visible-sm visible-md visible-lg">
      <li>Usted est&aacute; aqu&iacute;: <a href="index.php?page=home">Inicio</a></li>
      <li class="active">Reportes</li>
    </ol>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="">
          <h3><font color="167ac3"><strong>Reportes</strong></font></h3>
          <div id="">
            <div class="table-responsive" style="padding: 10px;">
              <table width="100%" border="0">

                  <?php
                  require_once("../modelos/db.php");
                  $userID = mareograficoDB::getInstance()->get_user_id_by_name($_SESSION["user"]);
                  $result = mareograficoDB::getInstance()->get_news_by_user_id($userID);



                  while ($row = mysqli_fetch_array($result)):
                      echo "<tr>";
                      echo"<td rowspan='8'>";
                      echo"<img src='vistas/imagenes/".$row['photo_img']."' width='150' height='161' />";

                      echo"</td>";
                      echo"<td>";
                      echo"<div id='titulo_noticia'><strong><br>".$row['title']."</strong></div>";
                      echo"</td>";
                      echo"</tr>";
                      echo"<tr>";
                      echo"<td>&nbsp;</td>";
                      echo"</tr>";
                      echo"<tr>";
                      echo"<td>";
                      echo"<div id='newsinfo' align='justify'>".$row['short_description']."</div>";
                      echo"</td>";
                      echo"</tr>";
                      echo"<tr>";
                      echo"<td>&nbsp;</td>";
                      echo"</tr>";
                      echo"<tr>";
                      echo"<td>";
                      echo"<div id='newsinfo' align='justify'>"."</div>";
                      echo"</td>";
                      echo"</tr>";
                      echo"<tr>";
                      echo"<td><br><div id='leer_mas'><a href='".$row['link1']."' target='_blank'><button type='button' class='btn btn-info'>LEER MAS</button></a></td>";
                      $newsID = $row["id"];
                      echo"</tr>";
                      echo"<tr>";
                      echo"<td></td>";
                      echo"</tr>";
                      echo"<tr>";
                      ?>
                    <td colspan="2">
                      <div id="boton_accion1">
                        <form name="deleteNews" action="vistas/deleteNews.view.php" method="POST">

                          <input type="hidden" name="newsID" value="<?php echo $newsID; ?>" />
                          <input type="submit" name="deleteNew" class="btn btn-danger btn-sm" value="Eliminar"  />

                        </form>
                      </div>
                      <div id="boton_accion2">
                        <form name="editNews" action="index.php?page=editNews" method="POST">
                          <input type="hidden" name="newsID" value="<?php echo $newsID; ?>" />
                          <input type="submit" name="editNew" class="btn btn-warning btn-sm" value="Editar" />
                        </form>
                      </div>
                    </td>
                    <td>

                    </td>
                      <?php

                      echo"</tr>\n ";
                  endwhile;
                  mysqli_free_result($result);
                  ?>

              </table><br>
            </div>
          </div>
          <hr>
          <a href="#modalNewNotice" class="btn btn-success" id="mantenimiento" data-toggle="modal" data-target="#modalNewNotice">
            <font color="#fff" >
              <strong>Agregar noticia</strong>

            </font>
          </a>
            <br><br><br><br><br><br>
        </div>
        <div class="modal fade" id="modalNewNotice"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                  <strong>Agregar una noticia</strong>
                </h4>
              </div>
              <div class="modal-body">
                <div class="contact_form2" id="contact_form2">
                  <form name="addNewNotice" method="POST" id="addNewNotice_form" action="vistas/insertNews.view.php" enctype="multipart/form-data" class="form-horizontal" >
                    <div class="form-group">
                      <label for="category" class="control-label col-xs-4">Categor&iacute;a:</label>
                      <div class="col-xs-4">
                        <select name="category" class="form-control">
                          <option value="publicaciones">Publicaciones y Noticias</option>
                          <option value="cursos">Cursos</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="titulo" class="control-label col-md-4">
                        T&iacute;tulo
                        <span class="required"> *</span>
                      </label>
                      <div class="col-md-6">
                        <input type="input" class="form-control" id="title" name="title" placeholder="T&iacute;tulo principal de la noticia"/>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="descripcion" class="control-label col-md-4">
                        Descripci&oacute;n
                        <span class="required"> *</span>
                      </label>
                      <div class="col-xs-8">
                        <textarea  name="description" id="description" rows="5" class="form-control" placeholder="Descripci&oacute;n de la noticia"></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-4" for="link">
                        V&iacute;nculo noticia
                        <span class="required"> *</span>
                      </label>
                      <div class="col-md-6">
                        <input type="input" class="form-control" id="link" name="link" placeholder="V&iacute;nculo a la noticia principal"/>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="control-label col-md-4">
                        <label for="imagen2">
                          Imagen
                          <span class="required"> *</span>
                        </label>
                      </div>
                      <div class="col-xs-8">
                        <input name="imagen" type="file"  />
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="col-md-6">

                        <input type="button" name="enviar" class="btn btn-info "  value="Insertar" onclick="addNoticeForm()">&nbsp;
                        &nbsp;&nbsp;&nbsp;<input type="reset" class="btn btn-info" value="borrar" />
                      </div>
                    </div>

                  </form>
                </div>
              </div><!-- End of Modal body -->
            </div><!-- End of Modal content -->
          </div><!-- End of Modal dialog -->
        </div><!-- End of Modal -->
      </div>
    </div>
  </div>
</div>