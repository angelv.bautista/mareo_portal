
<div class="wrapper">
  <ol class="breadcrumb visible-sm visible-md visible-lg">
      <li>Usted est&aacute; aqu&iacute;: <a href="index.php?page=home">Administraci&oacute;n de reportes</a></li>
    <li class="active">Instrucciones</li>
  </ol>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div id="paginationdemo" class="demo">

          <h3><font color="#167ac3"><strong>Instrucciones</strong></font></h3>
          <p align="justify">
            
            Bienvenidos a la secci&oacute;n de "administraci&oacute;n de reportes" del portal web del <strong>Servicio Meteorológico de la UNAM</strong>.
            Aqu&iacute; podr&aacute;s administrar el contenido de los reportes

          </p>
          <br>
          <h4><font color="#167ac3"><strong>Reporte de Eventos:</strong></font></h4>
          <p align="justify">
              En el men&uacute; de <i><b>REPORTES</b></i> podr&aacute;s accesar a los reportes y noticias
              del portal. En este módulo puedes <strong>agregar</strong> un nuevo reporte, <strong>editar</strong> o <strong>eliminar</strong>
              un reporte existente.<br><br>
              Para agregar un reporte deber&aacute;s llenar todos los campos que se piden (<strong>T&iacute;tulo</strong> del reporte,
              <strong>descripci&oacute;n</strong>, 
              <strong>v&iacutenculo</strong> al reporte original, as&iacute; como la <strong>categor&iacute;a</strong> a la que pertenece:
              <strong>reportes o noticias </strong> y subir su
              correspondiente <strong>foto</strong> con medidas de: <strong>150 pixeles de ancho x 160 pixeles de alto</strong>).
              <br><br>
              Podr&aacute;s visualizar los reportes existentes y elegir el que desees editar o eliminar con sus respectivos botones. Para
              editar un reporte basta con hacer clic en su bot&oacute;n correspondiente y editar el campo que necesitemos. Una vez echo alg&uacute;n
              cambio es necesario hacer clic en el bot&oacute;n <strong>"Actualizar"</strong> para <strong>"guardar"</strong> los cambios realizados al reporte.
              <br><br>
              Por &uacute;ltimo; para <strong>borrar</strong> un reporte existente basta con elegir el reporte a eliminar y hacer clic en el bot&oacute;n
              <strong>"eliminar"</strong>. Al hacer esto, nos aparecer&aacute; un cuadro de di&aacute;logo que nos preguntar&aacute; si estamos seguros
              de querer borrar el reporte seleccionado.
              
          
          </p>
        <br><br><br><br><br><br><br><br><br>
        
        
      </div>
    </div>
  </div>
</div>
