<div class="panel panel-default">
    <div class="panel-heading">
        ¡Bienvenidos!
    </div>
    <div class="panel-body">
        <div class="cb">  
        <p align="justify">
            Les damos una cordial bienvenida a la página de la 
            Red Universitaria de Observatorios Atmosféricos (RUOA).
            Aquí encontrarán información relevante sobre este proyecto que busca 
            ampliar la disponibilidad de datos atmosféricos y ambientales en estaciones
            albergadas en diferentes localidades con perfil académico del país.
        </p>
        <p align="justify">
            A través de una amplia cooperación entre las diversas instituciones 
            participantes...
        </p>
        <p align="right">
            <button type="button" id="enviar_alta2" value="Leer más" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal_alta2" />Leer más</button>
        </p>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal_alta2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">¡Bienvenidos!</h4>
            </div>
            <div class="modal-body">
                <div id="show_table_alta">
                    <p align="justify">
                        Les damos una cordial bienvenida a la página de la 
                        Red Universitaria de Observatorios Atmosféricos (RUOA).
                        Aquí encontrarán información relevante sobre este proyecto que busca 
                        ampliar la disponibilidad de datos atmosféricos y ambientales en estaciones
                        albergadas en diferentes localidades con perfil académico del país.
                    </p>
                    <p align="justify">
                        A través de una amplia cooperación entre las diversas instituciones participantes, 
                        se pone a la disposición de la comunidad científica y el público en general, esta 
                        información de consulta en tiempo real y para su libre descarga. Los diversos 
                        parámetros y observables pueden ser empleados tanto para fines educativos como 
                        para llevar a cabo investigaciones en meteorología, contaminación, cambio climático 
                        y disciplinas afines. Los invitamos a involucrarse activamente para mejorar el contenido
                        de esta única plataforma de información 
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">
                    Cerrar
                </button>
            </div>
        </div>
    </div>
</div>