<div id="imagen">
  <div class="row">
    <div class="col-xs-3 col-sm-3 col-md-2">
      <div id="ruoa_logo">
        <a class="banner-brand visible-sm visible-md visible-lg" href="http://www.ruoa.unam.mx/" target="_blank">
          <img class="img-responsive" src="img/logo-mareo.png" alt="Red de Universitaria de Observatorios Atmosféricos" >
        </a>
      </div>
    </div>
    <div class="col-xs-8 col-sm-8 col-md-6 visible-sm visible-md visible-lg ruoa_t">
        <img class="img-responsive" src="img/logo-geofisica.png" alt="Red de Universitaria de Observatorios Atmosféricos" >
    </div>
    <div class="col-md-4  visible-md visible-lg">
      <div id="unam">
        <a href="http://www.atmosfera.unam.mx/" target="_blank">
          <img class="img-responsive" src="img/logo-unam-a.jpeg"width="140px;" alt="Centro de Ciencias de la Atmósfera, UNAM" >
        </a>
      </div>
      <div id="unam">

      </div>
    </div>
  </div>
</div>