<?php
if ($_SERVER['REQUEST_METHOD'] === 'GET')
{
    $isStation = isset($_GET['station']) ? $_GET['station'] : false;
    if($isStation)
    {
        $idstation=strtolower($_GET['station']);
    }
    else
    {
        $idstation='jqrots' ;
    }
    $isfrom = isset($_GET['from']) ? $_GET['from'] : false;
    $isto = isset($_GET['to']) ? $_GET['to'] : false;

    if($isfrom )
    {
        if ($isto){
            $idfrom=$_GET['from'] ;
            $idto=$_GET['to'] ;}
    }


    include("connection.phtml");

    $link = server_connection();

    if ($link->connect_errno ) {

        die('No puede conectarse al servidor: ' . $link->connect_error);
    }
    else{

        $queryString= (string)  "select * from ".$idstation."   where fecha between '" .$idfrom."' and '".$idto."' order by fecha;" ;
        $sth = $link->query($queryString);

    }

    if (!$sth) {
        echo $queryString;
        echo '\n No se pudo ejecutar la consulta: ';
        exit;
    }


    $Station = new StationData();
    while($row = $sth->fetch_assoc())
    {
        $Station->fecha[]=date("Y-m-d H",strtotime($row['fecha']));
        $Station->temperatura[]= $row['temperatura'];
        $Station->humedad[]= $row['humedad'];
        $Station->v_viento[]= $row['velocidad'];
        $Station->d_viento[]= $row['direccion'];
        //$Station->ds_viento[]= $row['ds_viento'];
        $Station->lluvia[]=$row['lluvia'];
        $Station->presion[]=$row['presion'];
        $Station->radiacion[]=$row['radiacion'];
        $Station->visibilidad[]=$row['visibilidad'];
    }


    header('Content-type: application/json');

    echo  json_encode($Station);



}

class StationData {
    public $fecha;
    public $temperatura=array();
    public $humedad=array();
    public $v_viento=array();
    public $d_viento=array();
    //public $ds_viento=array();
    public $lluvia=array();
    public $presion=array();
    public $radiacion =array();
    public $visibilidad=array();


}
?>
