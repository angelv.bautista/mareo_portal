<?php
    require_once ("modelos/db.php");
    $logonSuccess = false;

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $logonSuccess = (mareograficoDB::getInstance()->verify_user_credentials($_POST['user'], $_POST['userpassword']));
        if ($logonSuccess == true) {
            session_start();
            $_SESSION['user'] = $_POST['user'];
            header('Location: reportes/index.php');
            exit;
        }
    }
    require_once ('controladores/common.php');
    $page = empty($_REQUEST['page']) ? 'home' : preg_replace('/[^a-zA-Z0-9\-_]/', '', $_REQUEST['page']);
    $pagepath = "controladores/$page.class.php";
    if (file_exists($pagepath)){
        require_once ($pagepath);
        $pageclass = $page . 'Class';
    }else{
        $pageclass = 'BasePage';
    }
    $mainframe = new $pageclass($page)
?>
<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">

        <title><?php $mainframe->title(); ?></title>
        <meta name="description" content="">

        <!-- MAIN'S CSS -->
        <link rel="stylesheet" href="resources/css/main.css">
        <script src="resources/js/jquery.min.js"></script>
        <link rel="shortcut icon" href="favicon.ico" />

        <!-- calendario -->
        <script src="resources/js/moment.min.js"></script>
        <script src="resources/js/jquery.hammer.js"></script>
        <link href="resources/css/caleran.min.css" rel="stylesheet"/>
        <script src="resources/js/caleran.min.js"></script>

        <!-- HIGHCHARTS -->
        <script src="resources/code/highcharts.js"></script>
        <script src="resources/code/modules/boost.js"></script>
        <script src="resources/code/modules/series-label.js"></script>
        <script src="resources/code/modules/exporting.js"></script>
        <script src="resources/code/modules/export-data.js"></script>

        <link rel="stylesheet" href="resources/css/slider.css">

    </head>
    <body id="<?php echo  $page; ?>_page">
        <header>
            <?php include ("vistas/plantillas/menu.php")?>
        </header>
        <main role="main">
                <?php $mainframe->render(); ?>
        </main>
        <footer>
            <div class="container-fluid">
                <?php include("vistas/plantillas/footer.php"); ?>
            </div>
        </footer>
        <?php include("modal.php");?>

        <script src="resources/js/plugins.js"></script>
        <script type="text/javascript" src="resources/js/sliderShowcase.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#slider1').SliderShowcase({
                    autoPlay:false,
                    slide:'horizontal',
                    pauseOnHover: 'true',
                    random: 'false'
                });
            });
        </script>
    </body>
</html>