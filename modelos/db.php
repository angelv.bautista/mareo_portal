<?php
class mareograficoDB extends mysqli{
    // instancia única autocompartida para todas las instancias
    private static $instance = null;

    // variables de configuración para conexión a la bd
    private $user = "dbcuenta03";
    private $pass = "Cta.m1sql03";
    private $dbName = "db03";
    private $dbHost = "localhost";

    // Este método debe ser estático, y debe retornar una instancia del objeto
    // si éste no existe
    public static function getInstance(){
        if(!self::$instance instanceof self){
            self::$instance = new self;

        }return self::$instance;

    }
    // los métodos clone y wakeup impiden la instanciación externa de la clase singleton,
    // eliminando así la posibilidad de objetos duplicados
    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('Deserializing is not allowed.', E_USER_ERROR);
    }
    //private constructor
    private function __construct() {
        parent::__construct($this->dbHost, $this->user, $this->pass, $this->dbName);
        if(mysqli_connect_error()){
            exit('Connect Error (' . mysqli_connect_errno() . ')'
                . mysqli_connect_error());

        }
        //parent::set_charset('utf-8');
    }
    public function verify_user_credentials ($name, $password){
        $name = $this->real_escape_string($name);

        $password = $this->real_escape_string($password);
        $result = $this->query("SELECT 1 FROM users WHERE name = '" . $name . "' AND password = '" . $password . "'");
        return $result->data_seek(0);
    }
    public function get_user_id_by_name($name) {

        $name = $this->real_escape_string($name);
        $user = $this->query("SELECT id FROM users WHERE name = '" .$name . "'");

        if ($user->num_rows > 0) {
            $row = $user->fetch_row();
            return $row[0];
        } else
            return null;
    }
    public function get_news_by_user_id($userID) {
        return $this->query("SELECT * FROM info_news WHERE user_id=" .$userID);
    }
    public function insert_notice($userID, $category, $title, $short_description, $photo, $vinculo){
        $category = $this->real_escape_string($category);
        $title = $this->real_escape_string($title);
        $short_description = $this->real_escape_string($short_description);
        $photo = $this->real_escape_string($photo);
        $vinculo = $this->real_escape_string($vinculo);

        $this->query("INSERT INTO info_news (user_id, category, title, short_description, photo_img, link1)" .
            " VALUES (" . $userID . ", '" . $category . "', '" . $title . "', '" . $short_description . "', '" . $photo . "', '" . $vinculo . "')");
    }
    public function delete_notice ($newsID){
        $this->query("DELETE FROM info_news WHERE id = " . $newsID);

    }
    public function update_news ($newsID, $category, $title, $short_description, $link){
        $newsID = $this->real_escape_string($newsID);
        $category = $this->real_escape_string($category);
        $title = $this->real_escape_string($title);
        $short_description = $this->real_escape_string($short_description);
        $vinculo = $this->real_escape_string($link);

        $this->query("UPDATE info_news SET category = '" . $category . "', title = '" . $title . "', short_description = '" . $short_description .
            "', link1 = '" . $vinculo . "' WHERE id = ". $newsID);

    }
    public function get_news_by_news_id ($newsID) {
        return $this->query("SELECT id, category, title, short_description, photo_img, link1 FROM info_news WHERE id = ". $newsID);
    }
    public function get_news_by_news_category () {
        return $this->query("SELECT id, title, short_description, photo_img, link1 FROM info_news WHERE category = 'publicaciones' ORDER BY id DESC");
    }
    public function get_news_by_news_category2 () {
        return $this->query("SELECT id, title, short_description, photo_img, link1 FROM info_news WHERE category = 'cursos'");
    }

    public function get_data_stations ($sId){
        $sId = $this->real_escape_string($sId);
        return $this->query("SELECT *  FROM stations where id=" .$sId);

    }


}
