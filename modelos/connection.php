<?php
    if (isset($_GET['id'])){
        $sId = $_GET['id'];

        //Validar los datos antes de que entren a la bd. En este caso, comprobar que el valor del parametro GET 'id' es
        // numerico
        if (is_numeric($sId) == true){
            try{

                //Verificar la conexion antes de ejecutar la consulta SQL
                //Configurar la conexion a la base de datos. ESto generalmente se llama manejador de base de datos
                //database handle (dbh)
                $dbh = new PDO('mysql:host=localhost;dbname=db03','dbcuenta03','Cta.m1sql03');

                //Usar PDO::ERRMODE_EXCEPTION, para capturar errores y escribirlos en un archivo de registro
                //para una inspeccion posterior en lugar de imprimirlos en la pantalla.
                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                /* Antes de ejecutar, preparar las declaraciones por parametros de enlace. Enlazar la entrada de usuario
                validada (en este caso, el valor de $sId) a la declaracion de SQL antes de enviarla al servidor de la
                base de datos.
                Esto corrige la vulnerabilidad de inyeccion SQL  */
                $q = "SELECT * FROM stations WHERE id = :id";

                //Preparar la cadena de consulta SQL
                $sth = $dbh->prepare($q);

                //Vincular los parametros a la variable declarada
                $sth->bindParam(':id',$sId);

                //Ejecutar sentencia
                $sth->execute();

                //Establecer FETCH_ASSOC para devolver una matriz con los datos de la consulta
                $result= $sth->fetch(PDO::FETCH_ASSOC);

                $stNombre=$result['nombre'];
                $stLatitud=$result['latitud'];
                $stLongitud=$result['longitud'];
                $stAltitud=$result['altitud']." m.s.n.m.";
                $stUbicacion=$result['direccion'];
                $stImgActual=$result['img_actual'];
                $stTipo=$result['tipo'];
                $stGaleria=$result['galeria'];


                $dbh = null;
            }
            catch(PDOException $e){
                error_log('PDOException - ' .$e->getMessage(), 0);
                http_response_code(500);
                die('Error estableciendo conexion con la base de datos');
            }
        }
        else{
            http_response_code(400);
            die('Error procesando una peticion mal formada');

        }
    }

