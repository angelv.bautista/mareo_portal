<div class="">
    <?php
    echo "<a id='station_image' href='".$stImgActual."' title=''>";
    echo "<img src='".$stImgActual."' class='img-responsive' />";
    echo "</a>";

    ?>
    <script type="text/javascript">
        var station_name = <?php echo json_encode($stNombre); ?>;
    </script>
    <table class="table table-bordered table-condensed">
        <tr>
            <td colspan="2" class="cell_center"><h2>Datos de la Estación</h2></td>
        </tr>
        <tr>
            <td class="cell_format1">
                Latitud:
            </td>
            <td class="cell_center">
                <?php echo $stLatitud ?>
            </td>

        </tr>
        <tr>
            <td class="cell_format1">
                Longitud:
            </td>
            <td class="cell_center">
                <?php echo $stLongitud ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="cell_format2">
                Ubicación
            </td>
        </tr>
        <tr>
            <td colspan="2" class="aire4">
                <br>
                <p style="text-align:justify;">
                    <?php echo $stUbicacion ?>
                </p>
            </td>
        </tr>
        <tr>
            <td class="cell_center_bold">
                <a href="<?php echo $stGaleria ?>" target="_blank"  onClick="window.open(this.href, this.target, 'width=740,height=680, scrollbars=yes, resizable=yes'); return false;">
                    Galería de fotos
                </a>
            </td>

        </tr>
    </table>
</div>