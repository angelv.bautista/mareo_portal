<div id="footer_bg">
    <div class="row">
        <div class="col-sm-6 col-md-6 visible-md visible-lg">
            <h3>Enlaces</h3>
            <ul>
                <li><a href="https://www.geofisica.unam.mx/igualdad/" target="_blank">Igualdad de género</a>  </li>
                <li><a href="http://www.transparencia.unam.mx/" target="_blank">Portal de transparencia universitaria</a></li>
            </ul>
        </div>
        <div class="col-sm-6 col-md-6 visible-md visible-lg centrado">
            <h3>Solicitud de datos</h3>
            <p>
                En caso de utilizar la información en una publicación favor de citar la fuente como:

                SMN (año actual):Universidad Nacional Autónoma de México, Instituto de Geofísica, Servicio Mareografico Nacional, México.
                Dirección electrónica:
                http://www.mareografico.unam.mx
            </p>

        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-6 col-sm-9 col-md-9 col-lg-9">
            <a href="#modalLogon" id="mantenimiento" data-toggle="modal" data-target="#modalLogon">
                    <span class="footer_labels">
                        Iniciar sesión
                    </span>
            </a>
        </div>
        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
            <div align="right">

                &nbsp;&nbsp;&nbsp;
                <a href="#modalCreditos" id="creditos" data-toggle="modal" data-target="#modalCreditos">
                    <span class="footer_labels">
                        cr&eacute;ditos
                    </span>
                </a>
                &nbsp;&nbsp;&nbsp;
            </div>
        </div>
    </div>
</div>