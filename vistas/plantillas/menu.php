<div id="imagen">
    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-2">
            <div id="mareo_logo">
                <a class="banner-brand visible-sm visible-md visible-lg" href="#" target="_blank">
                    <img class="img-responsive"  src="img/logo-mareo.png" alt="" >
                </a>
            </div>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-6 visible-sm visible-md visible-lg ruoa_t">
            <img class="img-responsive" src="img/logo-geofisica.png" alt="" >
        </div>
        <div class=" col-md-4 visible-md visible-lg">
            <div id="unam">
                <a href="#" target="_blank">
                    <img class="img-responsive" src="img/logo-unam-b.jpeg" alt="" >
                </a>
            </div>

        </div>
    </div>
</div>

<nav class="navbar navbar-default">
    <div class="container-fluid contenedor">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SMN</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php?page=home">INICIO <span class="sr-only">(current)</span></a></li>
                <li><a href="#">TRAYECTORIA</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ESTACIONES <span class="caret"></span></a>
                    <ul class="dropdown-menu contenedor">
                        <li><a href="index.php?page=estacion&id=1">La Paz</a></li>
                        <li><a href="index.php?page=estacion&id=2">Mazatlán, Sin.</a></li>
                        <li><a href="index.php?page=estacion&id=3">Puerto Vallarta, Jal.</a></li>
                        <li><a href="index.php?page=estacion&id=4">Manzanillo, Col.</a></li>
                        <li><a href="index.php?page=estacion&id=5">Lázaro Cárdenas, Mich.</a></li>
                    </ul>
                </li>
                <li><a href="index.php?page=NewsList">REPORTES</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">INFORMACION <span class="caret"></span></a>
                    <ul class="dropdown-menu contenedor">
                        <li><a href="index.php?page=estacion&id=1">
                                ¿Qué es la Marea?</a></li>
                        <li><a href="index.php?page=estacion&id=2">
                                ¿Cuál es el origen de la Marea?
                            </a></li>
                        <li><a href="#">
                                ¿Cómo actúa la fuerza que genera la marea?

                            </a></li>
                        <li><a href="#">
                                ¿Cómo se mide la Marea?
                            </a></li>
                        <li><a href="#">
                                ¿Qué son las “mareas vivas” y las “mareas muertas”?
                            </a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">CONTACTO</a></li>

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>