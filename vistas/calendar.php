<script type="text/javascript">
    var todate= new Date();
    var fromdate= todate.setDate( todate.getDate() -5 );

    var station = <?php echo json_encode($stNombre); ?>;



    var selectdate;
    var userLanguage = navigator.language || navigator.userLanguage;

    if(userLanguage == "en") userLanguage = "es";
    //Inicializar calendario
    var clendario=$("#caleran-ex-t-r").caleran({
        locale:userLanguage='es',
        showFooter: false,
        startDate:todate,
        endDate:fromdate,
        autoCloseOnSelect: true,
        showOn: "top",
        arrowOn: "right",
        onafterselect: function(elem,start,end) {
            var fromdatesel = start.format("YYYY-MM-DD HH:mm:ss");
            var todatesel = end.format("YYYY-MM-DD HH:mm:ss");

            document.getElementById('chart').setAttribute("style","height:600px");

            $("#chart").empty();

            getdata(station,fromdatesel,todatesel);
            return true;
        }
    });
</script>