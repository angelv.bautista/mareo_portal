<div style="padding-left: 10px; padding-top:40px; padding-bottom: 30px; width: 100%; text-align: center;">
    <label>&nbsp;&nbsp;Selecciona período:</label>
    <input type="text" id="caleran-ex-t-r" value="06/21/2017" /><br><br><br>
    <div id="chart"></div>
</div>
<?php include ("calendar.php"); ?>
<script type="text/javascript">
    $(document).ready(function() {
        fromdate="2018-10-21";
        todate="2018-10-26";
        //getdata(station, moment(fromdate).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'));
        getdata(station, fromdate, todate);
    });

    function getdata(station,fromdate,todate){
        //var elurl = "http://localhost/Chart/get_st_data.php?station="+station+"s&from="+fromdate+"&to="+todate;
        var url = "datos/get_st_data.php?station="+station+"&from="+fromdate+"&to="+todate;

        $.ajax({
            url: url,
            async: true,
            crossDomain : true,
            type: "GET",
            dataType: 'json',
            success: function(data) {
                var div = document.getElementById("chart");
                Draw(data,div,station,fromdate,todate);

            },
            error: function(ex) {
                console.log(ex);
                console.log('NOT!');
            }
        });
    }
    function Draw(data,div,station,fromdate,todate){

        var  serie1 = [];
        var  serie2 = [];
        var  serie3 = [];
        var  serie4 = [];
        var  serie5 = [];
        var  serie6 = [];
        var  serie7 = [];
        var  serie8 = [];
        var  serie9 = [];
        for(var value in data.temperatura)
        {
            serie1.push(Number(data.temperatura[value]));
            serie2.push(Number(data.humedad[value]));
            serie3.push(Number(data.v_viento[value]));
            serie4.push(Number(data.d_viento[value]));
            //serie5.push(Number(data.ds_viento[value]));
            serie6.push(Number(data.lluvia[value]));
            serie7.push(Number(data.presion[value]));
            serie8.push(Number(data.radiacion[value]));
            serie9.push(Number(data.visibilidad[value]));
        }

        Highcharts.chart({
            chart: {
                zoomType: 'xy',
                panning: true,
                panKey: 'shift',
                height: 550,
                renderTo: div
            },
            title: {
                text: 'Periodo: '+fromdate+' a  '+todate
            },
            xAxis: {
                categories: data.fecha,

                title: {
                    text: 'Fecha'
                }
                //labels:{
                //rotation: 90,
                //align: 'left'
                //}
            },
            yAxis:
                [
                    {
                        title:
                            {
                                text: '',
                                style:
                                    {
                                        color: "#ff0000"
                                    }
                            },
                        labels:
                            {
                                format: '{value} °C',
                                style:
                                    {
                                        color: "#ff0000"
                                    },
                            }
                    },
                    {
                        title:
                            {
                                text: '',
                                style:
                                    {
                                        color: "#2A55CE"
                                    }
                            },
                        labels:
                            {
                                format: '{value} %',
                                style:
                                    {
                                        color: "#2A55CE"                       }
                            },
                        opposite: true
                    },
                    {
                        title:
                            {
                                text: '',
                                style:
                                    {
                                        color: Highcharts.getOptions().colors[2]
                                    }
                            },
                        labels:
                            {
                                format: '{value} m/s',
                                style:
                                    {
                                        color: "#ffd700"
                                    }
                            }
                    },
                    {
                        gridLineWidth: 0,
                        title:
                            {
                                text: '',
                                style:
                                    {
                                        color: Highcharts.getOptions().colors[3]
                                    }
                            },
                        labels:
                            {
                                format: '{value} °',
                                style:
                                    {
                                        color: "#024a40"
                                    }
                            },
                        opposite: true
                    },
                    {
                        gridLineWidth: 1,
                        title:
                            {
                                text: '',
                                style:
                                    {
                                        color: Highcharts.getOptions().colors[4]
                                    }
                            },
                        labels:
                            {
                                format: '{value} mm',
                                style:
                                    {
                                        color: "#0B0080"
                                    }
                            }
                    },
                    {
                        title:
                            {
                                text: '',
                                style:
                                    {
                                        color: Highcharts.getOptions().colors[0]
                                    }
                            },
                        labels:
                            {
                                format: '{value}  hPa',
                                style:
                                    {
                                        color: "#000000"
                                    }
                            }
                    },
                    {
                        gridLineWidth: 0,
                        title:
                            {
                                text: '',
                                style:
                                    {
                                        color: Highcharts.getOptions().colors[6]
                                    }
                            },
                        labels:
                            {
                                format: '{value} W/m^2',
                                style:
                                    {
                                        color: "#ff790d"
                                    }
                            },
                        opposite: true
                    },
                    {
                        gridLineWidth: 0,
                        title:
                            {
                                text: '',
                                style:
                                    {
                                        color: Highcharts.getOptions().colors[7]
                                    }
                            },
                        labels:
                            {
                                format: '{value} m',
                                style:
                                    {
                                        color: "#03250d"
                                    }
                            },
                        opposite: true
                    }
                ],
            tooltip:
                {
                    shared: true
                },
            legend:
                {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                },
            credits: {
                enabled:false
            },
            exporting: {
                menuItemDefinitions: {
                    // Custom definition
                    label: {
                        onclick: function () {
                            this.renderer.label(
                                'Acabas de hacer click en un item personalizado You just clicked a custom menu item',
                                100,
                                100
                            )
                                .attr({
                                    fill: '#a4edba',
                                    r: 5,
                                    padding: 10,
                                    zIndex: 10
                                })
                                .css({
                                    fontSize: '1.5em'
                                })
                                .add();
                        },
                        text: 'Show label'
                    }
                },
                buttons: {
                    contextButton: {
                        menuItems: ['downloadPNG', 'downloadJPEG', 'downloadSVG', 'downloadPDF','downloadCSV']
                    }
                }
            },

            series:
                [
                    {
                        name: 'Temperatura',
                        tooltip:
                            {
                                valueSuffix: '°C'
                            },
                        data: serie1,
                        color: '#ff0000'
                    },
                    {
                        name: 'Humedad',
                        yAxis: 1,
                        tooltip:
                            {
                                valueSuffix: '%'
                            },
                        data: serie2,
                        color: "#2A55CE"
                    },
                    {
                        name: 'Precipitación Total',
                        yAxis: 4,
                        type: 'column',
                        tooltip: {
                            valueSuffix: 'mm'
                        },
                        data: serie6,
                        color: "#0B0080"
                    },
                    {
                        name: 'Rapidez de viento',
                        yAxis: 2,
                        tooltip: {
                            valueSuffix: 'm/s'
                        },
                        data: serie3,
                        color: "#ffd700",
                        visible: true
                    },
                    {
                        name: 'Dirección de viento',
                        yAxis: 3,
                        //marker: {
                        //  enabled: false
                        //},
                        dashStyle: 'shortdot',

                        tooltip: {
                            valueSuffix: '°'
                        },
                        data: serie4,
                        color: "#024a40",
                        visible: true
                    },

                    {
                        name: 'Presión',
                        yAxis: 5,
                        tooltip: {
                            valueSuffix: 'hPa'
                        },
                        data: serie7,
                        color: "#000000",
                        visible: false
                    },
                    {
                        name: 'Radiación',
                        yAxis: 6,
                        tooltip: {
                            valueSuffix: ' W/m^2'
                        },
                        data: serie8,
                        color: "#ff790d",
                        visible: false
                    },
                    {
                        name: 'Visibilidad',
                        yAxis: 7,
                        tooltip: {
                            valueSuffix: 'hPa'
                        },
                        data: serie9,
                        color: "#03250d",
                        visible: false
                    }
                ]
        });

    }
</script>
