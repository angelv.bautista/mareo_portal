<div class="responsive-container">

    <!-- <div id="head" class="container"> -->

    <section id="slider1">
        <div>

            <img data-info="" src="img/m1.png" alt=""/>
        </div>
        <div>

            <img data-info="" src="img/m2.png" alt=""/>
        </div>

    </section>
    <!-- </div> -->


</div>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-6">
            <br>
            <img class="img-responsive sombra" src="img/tuxpan3000001-scaled.jpg">
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6 centrado">
            <h1>Nosotros</h1>
            <p>

                El Servicio Mareográfico de la UNAM resguarda, documenta y analiza la información mareográfica de más de 50 años de mediciones en más de 30 localidades, y mantiene el monitoreo del nivel del mar en varios sitios del país. La medición del nivel del mar en la UNAM representa uno de los esfuerzos pioneros y más importantes de monitoreo de variables ambientales en forma operacional en México.
            </p>

        </div>

    </div>

</div>